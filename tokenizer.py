#!/usr/bin/python3

###################################################
################## exceptions #####################

class CktException(Exception):
    def __init__(self, line_index, character, message):
        self.ckt_line_index = line_index
        self.ckt_character = character
        super().__init__(f'[Chr "{character}" @ line {line_index}] {message}')

class MissingKeyException(CktException):
    def __init__(self, line_index, character):
        super().__init__(line_index,character,'Missing/empty key')

class UnexpectedNewlineException(CktException):
    def __init__(self, line_index, character):
        super().__init__(line_index,character,'Unexpected newline')

class UnexpectedEofException(CktException):
    def __init__(self, line_index, character):
        super().__init__(line_index,character,'Unexpected end of file')


###################################################
################### Tokenizer #####################
class Token():
    def __init__(self, _type, data=None):
        self.type = _type
        self.data = data
    
    def __str__(self):
        return f'<{self.type}:{self.data}>'


class Tokenizer():
    def __init__(self, source, debug=False):
        self.source = source + '\n'
        self.index = 0
        self.end_index = len(self.source)
        self.debug = debug
        self.last = None
    
    def print(self, *msg):
        if self.debug:
            print(f'[{self.line_number()}][{str(self.index):5}][debug] {"".join(msg)}')

    def get_line(self, index):
        return self.source.split('\n')[index]
    
    def line_number(self):
        return self.source[:self.index].count('\n')

    # def __iter__(self):
    #     return self
    
    # def __next__(self):
    #     return self.next()

    def curr_char(self):
        # prev = self.source[self.index-1]
        # self.print('curr:',curr,', prev:',prev)
        if self.reached_end():
            return ''
        return self.source[self.index]

    def reached_end(self):
        return self.index >= self.end_index

    def skip_to_newline(self):
        while self.curr_char() != '\n' and not self.reached_end():
            self.index += 1
        self.print('skipped to newline')

    def skip(self, skippables):
        while not self.reached_end():
            if self.curr_char() == '#':
                self.skip_to_newline()
            elif self.curr_char() in skippables:
                self.index += 1
            else:
                return

    def skip_whitespace(self):
        self.skip([' ', '\t', '\v', '\f', '\r'])

    def skip_whitespace_and_newlines(self):
        self.skip([' ', '\t', '\v', '\f', '\r', '\n'])

    def skip_whitespace_newlines_and_breaks(self):
        self.skip([' ', '\t', '\v', '\f', '\r', '\n', ';', ','])

    def read_quoted_string(self, qoute):
        start_index = self.index
        self.index += 1
        while not self.reached_end():
            char = self.curr_char()
            if char == qoute:
                self.index += 1
                self.print('read qouted string: ',self.source[start_index:self.index])
                return self.source[start_index:self.index]
            elif char == '\n':
                raise UnexpectedNewlineException(self.line_number(), self.curr_char())
            elif char == '\\':
                self.index += 1
            self.index += 1
        else:
            raise UnexpectedEofException(self.line_number(), self.curr_char())

    def read_unquoted_string(self):
        start_index = self.index
        while not self.reached_end():
            char = self.curr_char()
            if char in [ '\n', '=', ';', ',', ']' ]:
                end_index = self.index
                out = self.source[start_index:end_index]
                out.strip(' \t\v\f\r')
                self.print('read unqouted string: ',out)
                return out
            self.index += 1

    def read_multiline_string(self):
        self.print('read multiline string start')
        start_index = self.index
        end_index = self.index
        root = True

        while not self.reached_end():
            # self.print(self.curr_char())
            if root:
                end_index = self.index
                self.skip_whitespace()
                if self.curr_char() == '|':
                    root = False
                else:
                    self.print('read multiline string: ',self.source[start_index:end_index])
                    return self.source[start_index:end_index]
            else:
                if self.curr_char() == '\n':
                    root = True

            self.index += 1
        self.print('multiline string termination: ',self.source[start_index:end_index])
        return self.source[start_index:self.index]
        # raise UnexpectedEofException(self.line_number(), self.curr_char())


    def next(self):
        while not self.reached_end():
            self.print('step')
            self.skip_whitespace_and_newlines() # ignore whitespace and
            char = self.curr_char()

            if char == '[':
                out = Token('table_start')
            elif char == ']':
                out = Token('table_end')
            elif char == '=':
                raise MissingKeyException(self.line_number(), self.curr_char())
            else:
                value = {
                '\'': lambda: self.read_quoted_string('\''),
                '"': lambda: self.read_quoted_string('"'),
                '|': lambda: self.read_multiline_string(),
                }.get(char, lambda: self.read_unquoted_string())()

                self.skip_whitespace_and_newlines()
                # self.print('char:',char,' value:',str(value))
                char = self.curr_char()
                if char in (';',','):
                    out = Token('value', value)
                elif char == '=':
                    out = Token('key', value)
                else:
                # elif char == ']':
                    out = Token('value', value)
                    self.index -= 1

            self.index += 1
            self.print('step completed')
            self.last = out
            yield out
        self.print('reached end')
        return
 
