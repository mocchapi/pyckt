#! /usr/bin/python3
from tokenizer import Tokenizer
from parser import parse, replacify


###################################################
############## functions for users ################


# PARSES!!! ckt -> dict, returns dict
def loads(source, allow_lists=True):
    tokenizer = Tokenizer(source)
    out = parse(tokenizer.next(), allow_lists=allow_lists)
    return out


toDict = loads


# reverse-parses??? dict -> ckt, returns ckt string
def dumps(dict_, ascii_mode=False):
    return reverseparse(dict_, ascii_mode)

fromDict = dumps
fromList = dumps

def clean_string(strong):
    strong = unescapify(strong)
    # if strong in ['',' ', '\t', '\v', '\f', '\r'] or strong.startswith('#'):
    strong = '"'+strong+'"'
    return strong

def reverseparse(thing,ascii_mode,scope=0):
    prefix = '    '*scope
    out = [] 
    if type(thing) in [dict, list, tuple, set]:
        if type(thing) == dict:
            for key in thing:
                out.append(prefix+clean_string(key)+' = '+reverseparse(thing[key],ascii_mode=ascii_mode,scope=scope+1))
        else:
           for item in thing:
                out.append(prefix+reverseparse(item,ascii_mode=ascii_mode,scope=scope+1))
        if scope > 0:
            out.insert(0,prefix[:-8]+'[')
            out.append(prefix[:-4]+']')
        out.append('')

    elif type(thing) == str:
        # print(thing)
        # thing = unescapify(thing)
        thing = clean_string(thing)
        if ascii_mode:
            thing = asciify(thing)
        out.append(thing)
    else:
        if ascii_mode:
            thing = asciify(str(thing))
        out.append(clean_string(str(thing)))
    return '\n'.join(out)


def unescapify(string_):
    dict_ = {
        '\n': '\\n',
        '\v': '\\v',
        '\f': '\\f',
        '\r': '\\r',
        '\t': '\\t',
         '"': '\\"',
    }
    return replacify(string_, dict_)

def listprint(list_, prefix='||'):
    list_len = len(list_)
    for index, item in enumerate(list_):
        if index == list_len-1:
            cprefix = prefix[:-3]+'  '
            prefix = prefix[:-3]+' └└'
        else:
            cprefix = prefix
        
        if (x:= type(item)) == dict:
            print(prefix.replace('└','|'))
            print(prefix+'───',f'[{index}] ')
            # print(cprefix+'   |')
            if index < list_len - 1:
                if type(list_[index+1]) == dict:
                    dictprint(item, prefix=cprefix+'    |')
                else:
                    dictprint(item, prefix=cprefix+'    ||')
            else:
                dictprint(item, prefix=cprefix+'    ||')
            print(cprefix)
        elif x == list or x == tuple:
            print(prefix.replace('└','|'))
            print(prefix+'───',f'[{index}] ')
            listprint(item, prefix=cprefix+'    ||')
            print(cprefix)
        else:
            print(prefix+'──',f'"{item}",')

def dictprint(dict_, prefix='|'):
    dict_len = len(dict_.keys())
    for index, thing in enumerate(dict_.items()):
        if index == dict_len-1:
            cprefix = prefix[:-2]+' '
            prefix = prefix[:-2]+' └'
        else:
            cprefix = prefix
        item, value = thing
        if (x:= type(value)) == dict:
            print(prefix.replace('└','|'))
            print(prefix+'───',f'"{item}": ')
            # print(cprefix+'   |')
            dictprint(value, prefix=cprefix+'    |')
            print(cprefix)
        elif x == list or x == tuple:
            print(prefix.replace('└','|'))
            print(prefix+'───',f'"{item}" ')
            listprint(value, prefix=cprefix+'    ||')
            print(cprefix)
        else:
            # value = value.replace("\n","\\n")
            print(prefix+'──',f'"{item}": "{value}"')


def asciify(string_):
    out = ''
    for char_ in string_:
        orrd = ord(char_)
        if orrd > 127:
            a = str(hex(orrd))[2:]
            if len(a) <= 2:
                b = '0'*(2-len(a))+a
                out+= '\\x'+b
            elif 4 >= len(a) > 2:
                b = '0'*(4-len(a))+a
                out+= '\\u'+b
            else:
                b = '0'*(8-len(a))+a
                out+= '\\U'+b
        else:
            out += char_
    return out

if __name__ == '__main__':

    with open('SPEC_testing.ckt', 'r') as f:
        spec = f.read()
    out = loads(spec, allow_lists=True)
    # dictprint(out)
    with open('SPEC_generated.ckt','w') as f:
        out2 = dumps(out,ascii_mode=True)
        f.write(' generated from the spec using pyckt '.center(100,'#')+'\n\n\n'+out2)
    out3 = loads(out2)
    print(out == out3)
    out4 = dumps(out3)
    # print(out3 == out4)
    print(out4 == out2)
    # print(out4)
    # print(out['equivelant to list style table'])
    # print(out['list test'])
    # print(out.keys())
    # print(out['singleline multiline string'])
    # print(out['escaped escape hatches'])
    # boppo = 'the quick brwon 𝄠𝄠 uwu ÀÀÀdXD'
    # print(asciify(boppo))
    # print(handle_hexes(asciify(boppo)))
    
    # for key, value in loads(spec).items():
    #     print(key,value)

    # import time

    # print(chr(27) + "[2J")
    # print('Testing tokeniser on spec'.center(80,'='))
    # with open('SPEC_runnable.ckt', 'r') as f:
    #     spec = f.read()
    
    # start_time = time.time()
    # tokenizer = Tokenizer(spec)
    # tokens = tokenizer.next()
    # # for token in tokens:
    # #     print(token)
    # end_time = time.time()
    # print(f'Done testing tokeniser in {end_time - start_time}s'.center(80,'='))
    # print('Testing parser on spec'.center(80,'='))
    # start_time = time.time()
    # print(parse(tokens))
    # end_time = time.time()
    # print(f'Done testing parser in {end_time - start_time}s'.center(80,'='))
