# pyckt
python3 ckt parser 
___
proper readme coming: not anytime soon >:3€

based on [zckt](https://git.sr.ht/~cricket/zckt)

reasonably close to functional
lacking the following:
 - any proper testing
 - probably some unknown parsing stuff i forgot about
 - informative exceptions at every turn 
 - **robust** exporting of lists/dicts to ckt 

its more or less usable for importing ckt files


## ok so how does it work anne
if youre familiar with how the `json` library works, good news, its basically the same.

use ```pyckt.loads(ckt_string)```   
with a string of ckt and you'll receive a dictionary with all the keys.
you may also use the maybe slightly more sensibly named `toDict` alias, if you want ;]

loads has a single kwarg, `allow_lists` which defaults to `True`. If set to true, list-like tables which dont have any custom keys will be interpreted as lists. Theres some more information about this at the bottom of the SPEC_runnable.ckt file. Setting this to `False` will obviously mean lists will not occur in the output.


You can export a dictionairy or list/tuple/set with `pyckt.dumps()` or the alias' `pyckt.fromDict()` or `pyckt.fromList()`. There are two arguments: the first any standard object you want to have turned into ckt, the second is the kwarg `ascii_mode`, which will turn any non-ascii symbol into its hex code counterpart. Not super useful but funky nontheless.

The export code is kinda funky and ive done not a lot of testing, its 6 am




tldr;
```myDict = pyckt.loads(some_ckt_string, allow_lists=True)``` 

```myCkt = pyckt.dumps(some_list_or_dict, ascii_mode=False)``` 
