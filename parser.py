#!/usr/bin/python3
import codecs 

# todo: code cleanup, make dumps() break less


def replacify(string_, dict_):
    for key,value in dict_.items():
        if string_.count(key) > 0:
            string_ = string_.replace(key,value)
    return string_

def escapify(string_):
    dict_ = {
        '\\n': '\n',
        '\\v': '\v',
        '\\f': '\f',
        '\\r': '\r',
        '\\t': '\t',
        '\\"': '"',
        '\\\\"': '\\',
    }
    return replacify(string_, dict_)

def strippify(string_):
    return string_.strip(' \t\v\r\f')

def is_quoted(string_):
    try:
        return [string_[0], string_[-1]] in [["'", "'"],['"','"']]
    except:
        return False

def is_multiline(string_):
    return string_.lstrip(' \t\v\r\f')[0] == '|'

def unquotify(string_):
    if is_quoted(string_):
        return string_[1:-1]
    return string_

def handle_hexes(string_):
    #im fucking gay dude
    indx = 0
    end = len(string_)
    out = ''
    while indx < end-2:
        char = string_[indx]
        nextchar = string_[indx+1]
        if char == '\\':
            # print('blop')
            if nextchar == 'x':
                # print('bloop',)
                out += chr(int(string_[indx+2:indx+4],16))
                indx+=3
            elif nextchar == 'u':
                out += chr(int(string_[indx+2:indx+6],16))
                indx+=5
            elif nextchar == 'U':
                out += chr(int(string_[indx+2:indx+10],16))
                indx+=9
            else:
                out+=char
        else:
            out+=char
        indx+=1
    out+=string_[-2:]
    return out

def handle_multiline(string_):
    dict_ = {
        '\\v': '\v',
        '\\f': '\f',
        '\\r': '\r',
        '\\t': '\t',
        '\\"': '"',
        '\\\\"': '\\',
    }
    out = ''
    lines = string_.split('\n')
    i = 0
    while i <= string_.count('\n'):
        line = lines[i]
        if line:
            grr = line.strip(' \t\v\r\f')
            if grr[0] == '|':
                if grr.replace('\\\\','').endswith('\\'):
                    # print('yeah:',line)
                    grr = grr[:-1].rstrip(' \t\v\r\f')
                else:
                    # print('nah:',line)
                    grr += '\n'
                # print(grr[])
                out += grr[1:]

        i += 1
            
    # else:
    #     grr = string_.lstrip(' \t\v\r\f')
    #     if grr[0] == '|':
    #         out.append(grr[1:])
    return replacify(out.rstrip('\n'), dict_)


def clean_string(string_):
    out = handle_hexes(string_)
    if is_multiline(out):
        out = handle_multiline(out)
        # out = strippify(out)
    else:
        out = strippify(out)
        if is_quoted(out):
            out = unquotify(out)
        out = escapify(out)

    return out


def dict_list_unify(item1, item2):
    type1 = type(item1)
    type2 = type(item2)
    types = [type1, type2]
    if dict in types:
        if list in types:
            if type1 == list:
                return item1
            else:
                return item2
        else:
            return {**item1, **item2}
    else:
        return item1 + item2


def parse(tokengen, allow_lists=True):
    out = {}
    key = None
    value = None
    is_list = allow_lists
    i = 0
    for token in tokengen:
        # print(token)
        if token.type == 'table_start':
            # print('table start')
            if key == None:
                key = str(i)
                i += 1
            if allow_lists:
                out[key] = dict_list_unify(out.get(key,{}), parse(tokengen, allow_lists=allow_lists))
            else:
                out[key] = {**parse(tokengen, allow_lists=allow_lists), **out.get(key, {})}
            key = None
        
        elif token.type == 'table_end':
            # print('table end')
            if is_list:
                return list(out.values())
            return out

        elif token.type == 'key':
            if is_list:
                try:
                    if int(token.data) == i:
                        i += 1
                    else:
                        is_list = False
                except:
                    is_list = False
            # print('key:',token.data)
            key = unquotify(strippify(token.data))

        elif token.type == 'value':
            if token.data == None:
                continue
            # print('value:',token.data)
            value = clean_string(token.data)
        
        if value:
            if key == None:
                key = str(i)
                i += 1
            out[key] = str(value)
            value = None
            key = None
    if is_list:
        return list(out.values())
    return out

# print('>'+handle_hexes('sajfklsa \\x41\\x6E\\x6E\\x65 is my name')+'<')
# print('>'+handle_hexes('aaaa \\u0041\\u006e\\u006e\\u0065 is my name too')+'<')
